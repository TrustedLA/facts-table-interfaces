module.exports = (config) => {
    config.set({
      basePath: '.',
      files: [
        './specs/*.js'
      ],
      frameworks: [ 'browserify','jasmine'],
      preprocessors: {
        './specs/*.js': [ 'browserify' ]
     },
     browserify: {
        debug: true
      },
     plugins: [ 'karma-browserify', 'karma-chrome-launcher', 'karma-jasmine']

    });
  }