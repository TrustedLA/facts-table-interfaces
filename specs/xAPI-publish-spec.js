const frisby = require('frisby');
const testHost = 'http://localhost:8080';

it('should accept an xAPI instance', function (doneFn) {
    frisby.post(testHost + '/xAPI/statements', {
        body: {
            "actor": {
                "mbox": "mailto:stefaan.ternier@gmail.com",
                "mbox_sha1sum": "stefaan.ternier@gmail.com",
                "name": "Stefaan Ternier",
                "objectType": "Agent"
            },
            "verb": {
                "id": "http://adlnet.gov/expapi/verbs/answered",
                "display": {
                    "en-US": "answered"
                }
            },
            "object": {
                "id": "http://adlnet.gov/expapi/activities/example",
                "definition": {
                    "name": {
                        "en-US": "Stefaan Ternier"
                    },
                    "description": {
                        "en-US": "Example activity description"
                    }
                },
                "objectType": "Activity"
            }
        }
    })
        .expect('status', 200)
        .then(function (res) { 
            let json = res.body;
            expect(json.length).toBe(1);
            expect(typeof json[0]).toBe('string');
        })
        .done(doneFn);

});
