var restify = require('restify');

const server = restify.createServer({
    name: 'xAPI-rest-server',
    version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

server.get('/echo/:name', function (req, res, next) {
    
        res.send({ name: req.params.name });
        return next();
    });

server.post('xAPI/statements', (req, res, next) => {
    res.send(['somefakeId']);
    next();

});

server.opts('xAPI/statements', function (req, res, next) {
    res.send(200);
    next();
});

server.listen(8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});